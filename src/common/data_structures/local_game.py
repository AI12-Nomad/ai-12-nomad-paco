import uuid as uuid_lib
from typing import List

from .profiles import Profile, Player
from .square import Square
from .message import Message
from .move import Move


class LocalGame:
    """
    Data class containing the game, not intended to be sent through the network.
    """

    def __init__(
        self,
        name: str,
        tiles_remaining: int,
        nb_towers: int,
        status: str,
        red_player: Player,
        black_player: Player,
        game_creator: Player,
        squares_list: List[List[Square]],
        game_round: int,
        chat: Message,
        game_uuid: uuid_lib.UUID = uuid_lib.uuid4(),
    ):
        self.game_round = game_round
        self.chat = chat
        self.squares_list = squares_list
        self.game_creator = game_creator
        self.black_player = black_player
        self.red_player = red_player
        self.status = status
        self.nb_towers = nb_towers
        self.tiles_remaining = tiles_remaining
        self.name = name

    def __str__(self):
        return f"LocalGame : {self.name}"
