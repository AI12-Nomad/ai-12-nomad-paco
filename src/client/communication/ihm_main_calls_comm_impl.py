from common import I_IHMMainCallsComm
from networking import create_client
from uuid import UUID
from common import LocalGame, Profile


class IHMMainCallsComm_Impl(I_IHMMainCallsComm):
    def __init__(self, controller) -> None:
        super().__init__()
        self.comm_controller = controller

    def remove_game(self, game_id: UUID) -> None:
        pass

    def get_profile_info(self, user_id: UUID) -> None:
        pass

    def create_game(self, local_game: LocalGame) -> None:
        pass

    def update_profile(self, profil: Profile) -> None:
        pass

    def spectate_game(self, user_id: UUID, game_id: UUID) -> None:
        pass

    def join_game(self, user_id: UUID, game_id: UUID) -> None:
        pass

    async def connect_to_server(self, ip: str, port: int) -> None:
        pass

    def disconnect_server(self) -> None:
        pass
