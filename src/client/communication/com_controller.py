from networking import IO
from ihm_game_calls_comm_impl import IHMGameCallsComm_Impl
from ihm_main_calls_comm_impl import IHMMainCallsComm_Impl
from common import I_IHMGameCallsComm, I_IHMMainCallsComm
from common import I_CommCallsIHMGame, I_CommCallsData, I_CommCallsIHMMain


class ComControllerClient:
    """
    ComController class

    """

    def __init__(self):
        self.__impl_interface_for_ihm_main = IHMGameCallsComm_Impl(self)
        self.__impl_interface_for_ihm_game = IHMMainCallsComm_Impl(self)
        self.__impl_comm_calls_ihm_game = None
        self.__impl_comm_calls_ihm_main = None
        self.__impl_comm_calls_data = None
        self.__callback_map = None
        self.io = None

    def get_interface_for_ihm_main(self) -> I_IHMMainCallsComm:
        return self.__impl_interface_for_ihm_main

    def get_interface_for_ihm_game(self) -> I_IHMGameCallsComm:
        return self.__impl_interface_for_ihm_game

    def set_interface_from_ihm_game(self, comm_calls_ihm_game: I_CommCallsIHMGame):
        self.__impl_comm_calls_ihm_game = comm_calls_ihm_game

    def set_interface_from_data(self, comm_calls_data: I_CommCallsData):
        self.__impl_comm_calls_data = comm_calls_data

    def set_interface_from_ihm_main(self, comm_calls_ihm_main: I_CommCallsIHMMain):
        self.__impl_comm_calls_ihm_main = comm_calls_ihm_main

    def set_callback_map(self) -> None:
        "a faire quand on aura la connectio faite"
        pass

    def set_connexion(self, connexion: IO) -> None:
        self.io = connexion
