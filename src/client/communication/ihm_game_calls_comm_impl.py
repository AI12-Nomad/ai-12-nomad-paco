from common import I_IHMGameCallsComm
from uuid import UUID
from common import Move, Message


class IHMGameCallsComm_Impl(I_IHMGameCallsComm):
    def __init__(self, controller) -> None:
        super().__init__()
        self.comm_controller = controller

    def send_message(self, message: Message) -> None:
        pass

    def place_tile(self, move: Move) -> None:
        pass

    def quit_spactator_interface(self, user_id: UUID, game_id: UUID) -> None:
        pass
